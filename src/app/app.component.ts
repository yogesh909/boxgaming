import { Component } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'GamingApp';
  endIndexForLevel: any = 1;
  currentlevel: any = 1;
  resultArray: any = [];
  isWrongAnswerNotClicked: boolean = true;
  systemInputArray = [];
  clickedIndex = 0;
  message = 'Hit Play to start level';
  flag = false;

  constructor() { }

  async startGameAnimation() {
    this.flag = true;
    this.systemInputArray = [];
    let divArray = ["div.yellowBox", "div.blueBox", "div.redBox", "div.greenBox"];

    for (var i = 0; i < this.endIndexForLevel; i++) {
      let currentHighlightArrayIndex = Math.floor(Math.random() * 4);
      this.systemInputArray.push(divArray[currentHighlightArrayIndex]);
      $(document).ready(() => {
        $(divArray[currentHighlightArrayIndex]).effect("highlight", { color: '#000000', easing: 'easeInElastic' }, 300);
      });
      await this.timer(500); // then the created Promise can be awaited
    }
    this.endIndexForLevel = this.endIndexForLevel + 1;
    this.message = "Please repeat the pattern by clicking on rectangles";
  }

  timer(ms) {
    return new Promise(res => setTimeout(res, ms));
  }

  onRectClick(clickedColor) {

    if (this.clickedIndex == this.endIndexForLevel - 1) {
      return;
    }

    if (!(clickedColor == this.systemInputArray[this.clickedIndex])) {
      this.isWrongAnswerNotClicked = false;
    }

    this.clickedIndex++;
    if (this.clickedIndex == this.endIndexForLevel - 1) {
      // console.log('-----' + this.clickedIndex);
      // console.log('-----' + this.endIndexForLevel);
      if (this.isWrongAnswerNotClicked) {
        console.log("correect answerrrrr");
        alert("Success - Level Increased");
        this.message = 'Hit Play to start level';
        this.flag = false;
        this.clickedIndex = 0;
        this.isWrongAnswerNotClicked = true;
        this.currentlevel++;
        if (this.currentlevel == 11) {
          this.currentlevel = 1;
          this.systemInputArray = [];
          this.endIndexForLevel = 1;
        }
      } else {
        console.log("wrongg answerrrrr");
        alert("Failure - Level Set to 1");
        this.message = 'Hit Play to start level';
        this.flag = false;
        this.clickedIndex = 0;
        this.isWrongAnswerNotClicked = true;
        this.currentlevel = 1;
        this.systemInputArray = [];
        this.endIndexForLevel = 1;
      }
      return;
    }
  }
}
